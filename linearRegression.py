# Source: https://www.tensorflow.org/get_started/
# Modified by Vivek Notani, Feb 6th, 2017
import sys
import os
import tensorflow as tf
import numpy as np

import data_in


print "Reading Input Data"
# Setup training data
data_path = 'example/trace2.plt'
if len(sys.argv)>1:
    data_path = os.path.abspath(sys.argv[1])

print "Importing trace from %s" % data_path
(var_count, x_data, y_data) = data_in.getData(data_path)

print "x: ", x_data
print "y[0]: ", y_data[0]

# Create 100 phony x, y data points in NumPy, y = x * 0.1 + 0.3
#x_data = np.random.rand(100).astype(np.float32)
#y_data = x_data * 0.1 + 0.3

print "Constructing Model"
W=[]
b=[]
y=[]
loss=[]

train=[]
for i in range(var_count):
    # Try to find values for W and b that compute y_data = W * x_data + b
    W.append(tf.Variable(tf.random_uniform([1], -1.0, 1.0)))
    b.append(tf.Variable(tf.zeros([1])))
    y.append( W[i] * x_data + b[i])

    # Minimize the mean squared errors.
    loss.append(tf.reduce_mean(tf.square(y[i] - y_data[i])))
    optimizer=tf.train.GradientDescentOptimizer(0.2)
    train.append(optimizer.minimize(loss[i]))

# Before starting, initialize the variables.  We will 'run' this first.
init = tf.global_variables_initializer()

print "learning"
# Launch the graph.
sess = tf.Session()
sess.run(init)

# Fit the line.
for step in range(201):
    for i in range(var_count):
        sess.run(train[i])
        if step % 20 == 0 and step >0:
            print(step, sess.run(W[i]), sess.run(b[i]))

# Learns best fit is W: [0.1], b: [0.3]

