# Created by Vivek Notani, Feb 6th, 2017

import numpy as np
import csv
import os, sys

def getValues(db_path):
    db = open(db_path,'r')
    # import the csv file
    reader = csv.reader(db, delimiter=',')
    # skip first empty record 
    next(reader)
    # get number of variables and skip this erroneous record (1st actual record is known to be errorneous in our generated traces)
    columns=len(next(reader)) -1 # trailing comma removed later
    # add iterator to first column
    i = 0
    for row in reader:
        # remove the extra comma
        row = row[:-1]
        i=i+1
        # Format to keep only values and yield
        if len(row) == columns :
            row_ = [int(i)]
            row_.extend(map(lambda s: int(s.split('=')[1]),row))
        else:
            continue    
        yield row_

def getData(data_path):
    data = getValues(data_path)
    first_row = data.next() 
    count = 1
    # subtract 1 column for iterator
    var_count = len(first_row) - 1
    
    x_data = np.array([first_row[0]],dtype=np.int32).reshape(1)
    y_data = np.array([first_row[1:]],dtype=np.int32).reshape(1,var_count)
    
    while(True):
        try:
            next = data.next()
            x_data = np.append(x_data,next[0])
            y_data = np.append(y_data,[next[1:]],axis=0)
            count += 1
        except StopIteration:
            break
    # Transpose y_data to get row vectors to represent values for each variable
    y_data = y_data.T
    print "%d records read. " % count
    return (var_count,x_data,y_data)

def writeCSV(out_path):
    data=getValues(db_path)
    csv=open(out_path,'w')
    while(True):
        try:
            next = data.next()
            csvString = ",".join(map(lambda x:str(x),next))+'\n'
            csv.write(csvString)
        except StopIteration:
            break
    csv.close()    


if __name__=='__main__':
    # usage python data_in.py [input-file] [outfile]
    # Setting defaults
    db_path = 'input/test3-12.plt'      # default input path

    if len(sys.argv)>1:
        db_path = os.path.abspath(sys.argv[1])

    out_path = os.path.abspath(os.path.join(db_path.split(".")[-2]+".csv"))
    if len(sys.argv)>2:
        out_path=sys.argv[2]

    writeCSV(out_path)    
        
