Steps:

1. Create a C program that ends with an exit(0)
2. compile with debug info: gcc -g <myprog.c> -o <myprog.out>
3. Make sure the program exits normally: ./<myprog.out> should terminate
4. Collect trace: expect -f autogdb.exp <myprog.out> >trace.log
5. Look up the line number in trace output whose state you want to learn
6. Extract trace for specific program line no and cleanup: awk '/<lineNo>\t/{flag=1;next}/next/{flag=0}flag' trace.log|awk '/info locals/{flag=1;print "";next}/gdb/{flag=0} {if (flag==1) printf substr($0, 1, length($0)-1)","}' >trace.plt

Warning: For each statement, GDB outputs the state before executing the statement !

7. python data_in.py trace.plt : creates a csv file with same name in the input file directory
8. Change the CSV file Reader config in KNIME framework-LinearLearning workflow.
9. Execute the CSV Reader node, followed by all the linear regression learner nodes.
10. Right click on the Linear Regression nodes to view the graph and coefficients learnt.
